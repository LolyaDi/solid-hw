﻿using System;
using System.Collections.Generic;

namespace DIP
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public interface IRepository<T>
    {
        IList<T> FindAll();
    }

    public class CustomerRepository : IRepository<Customer>
    {
        public IList<Customer> FindAll()
        {
            return new List<Customer>
            {
                new Customer { Id = 1, Name = "Someone1" },
                new Customer { Id = 2, Name = "Someone2" }
            };
        }
    }

    public class CustomerService
    {
        private IRepository<Customer> _customerRepository;

        public CustomerService(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public IList<Customer> GetAll()
        {
            return _customerRepository.FindAll();
        }
    }

    public class Program
    {
        static void Main(string[] args)
        {
            var service = new CustomerService(new CustomerRepository());

            Console.WriteLine("List of customers:");

            foreach (var customer in service.GetAll())
            {
                Console.WriteLine($"\tId: {customer.Id}, Name: {customer.Name}");
            }

            Console.ReadLine();
        }
    }
}
